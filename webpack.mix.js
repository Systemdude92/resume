let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
  .extract(['jquery', 'vue', 'axios'])
  .copyDirectory('resources/semantic', 'public/semantic')
  .copy('resources/assets/img/profilePhoto.jpg', 'public/images/profilePhoto.jpg')
  .version()
  .webpackConfig({
    module: {
      rules: [
        {
          test: /\.jpg$/,
          use: 'file-loader'
        }
      ]
    }
  })
  .browserSync({
    proxy: 'localhost:8000'
  });
