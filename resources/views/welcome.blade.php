<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Resume - {{$title}}</title>
        <meta name="author" content="Logan McCourry">
        <meta name="Description" content="Resume Site built in Laravel 5 and Vue for Logan McCourry">
        <meta name="keyword" content="resume, personal, logan, mccourry, loganmccourry">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{asset('semantic/dist/semantic.min.css')}}" type='text/css' rel='stylesheet' /> 
        <script src="{{mix('js/manifest.js')}}" rel="script" type='text/javascript'></script>
        <script src="{{mix('js/vendor.js')}}" rel='script' type='text/javascript'></script>
        <style>
            
            #app > div > .eight.wide.column {
                border-left: solid 1px black;
            }

            #app > div > .four.wide.column {
                padding-right: 5px;
            }

            @media only screen and (max-width: 425px){
                #app > div > .eight.wide.column {
                    border-left: solid 0 black;
                }
            }
        </style> 
        @yield('head')
    </head>
    <body>
        <div id='app' style='padding-top: 10px;'>
            <div class='ui centered stackable grid'>
                <div class='four wide column'>
                    <info-component></info-component>
                </div>
                <div class='eight wide column'>
                    <resume-component></resume-component>
                </div>
            </div>
        </div>
        <script src='{{mix('js/app.js')}}' type='text/javascript' rel='script'></script>
    </body>
</html>